#SpreadJS-Mobile-Terminal-Demo


SpreadJS是全平台的HTML5前端控件，在PC、Pad以及智能手机上都可以流畅使用。 

1. SpreadJS可以自适应屏幕大小，以响应式的方式呈现数据
2. SpreadJS针对触屏进行优化，方便移动端使用

官网地址：http://www.gcpowertools.com.cn/products/spreadjs/

下载链接：http://www.gcpowertools.com.cn/products/download.aspx?pid=57

在线演示：http://www.gcpowertools.com.cn/products/spreadjs/demo.htm

中文社区：http://gcdn.gcpowertools.com.cn/


![葡萄城控件微信服务号](http://www.gcpowertools.com.cn/newimages/qrgrapecity.png "葡萄城控件微信服务号")